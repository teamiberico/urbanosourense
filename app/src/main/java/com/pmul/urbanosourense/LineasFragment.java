package com.pmul.urbanosourense;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pmul.urbanosourense.model.Linea;
import com.pmul.urbanosourense.model.ListasApplication;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class LineasFragment extends Fragment {

    private RecyclerView listaLineas;
    private LineasListAdapter lineasListAdapter;
    private ActionBar actionBar;

    public LineasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lineas, container, false);

        actionBar = ((MainActivity)getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Lineas");
            actionBar.setSubtitle("");
        }

        listaLineas = view.findViewById(R.id.lineasListRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        listaLineas.setLayoutManager(layoutManager);
        listaLineas.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Fragment fragment = new FavoritosFragment();
                Bundle args = new Bundle();
                args.putString("origen", "busqueda");
                args.putString("resultado",query);
                fragment.setArguments(args);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ArrayList<Linea> lineas;

        if (getArguments().getString("origen").equals("favoritos")) {
            lineas = ListasApplication.getLineasRepository().getLineasFavoritas();
            setHasOptionsMenu(false);
            MainActivity.borrarLinea = true;

            if (actionBar != null) {
                actionBar.setTitle("Favoritos");
                actionBar.setSubtitle("");
            }
        }
        else if(getArguments().getString("origen").equals("mapa")){
            lineas = ListasApplication.getLineasRepository().getLineas(ListasApplication.getPoseeRepository().getNumeroLinea(getArguments().getString("codigo")));
            setHasOptionsMenu(true);
            MainActivity.borrarLinea = false;
        }
        else if (getArguments().getString("origen").equals("busqueda")){
            lineas = ListasApplication.getLineasRepository().getLineas(getArguments().getString("resultado"));

            if (lineas.size() == 0){
                Toast.makeText(getContext(),"No se ha encontrado ninguna linea",Toast.LENGTH_SHORT).show();
            }

            setHasOptionsMenu(false);
            MainActivity.borrarLinea = false;

            if (actionBar != null) {
                actionBar.setTitle("Resultados");
                actionBar.setSubtitle("");
            }
        }
        else {
            lineas = ListasApplication.getLineasRepository().getLineas();
            setHasOptionsMenu(true);
            MainActivity.borrarLinea = false;
        }

        lineasListAdapter = new LineasListAdapter(lineas);
        listaLineas.setAdapter(lineasListAdapter);
        listenerLinea();
    }

    public void listenerLinea() {
        lineasListAdapter.setOnClickListener(new LineasListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, Linea linea) {
                ParadasFragment fragment = new ParadasFragment();
                Bundle args = new Bundle();
                args.putString("numeroLinea", linea.getNumeroLinea());
                args.putString("origenLinea",linea.getOrigen());
                args.putString("destinoLinea",linea.getDestino());
                args.putString("origen", "lineas");
                fragment.setArguments(args);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
            }
        });
    }

}
