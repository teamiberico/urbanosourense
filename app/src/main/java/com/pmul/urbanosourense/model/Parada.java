package com.pmul.urbanosourense.model;

/**
 * Created by Noel on 04/11/2017.
 */

public class Parada {

    private String codigoParada;
    private String nombreParada;
    private Double latitud;
    private Double longitud;
    private boolean favoritoParada;

    public Parada () {}

    public Parada(String codigoParada, String nombreParada, Double latitud, Double longitud, boolean favoritoParada) {
        this.codigoParada = codigoParada;
        this.nombreParada = nombreParada;
        this.latitud = latitud;
        this.longitud = longitud;
        this.favoritoParada = favoritoParada;
    }

    public String getCodigoParada() {
        return codigoParada;
    }

    public void setCodigoParada(String codigoParada) {
        this.codigoParada = codigoParada;
    }

    public String getNombreParada() {
        return nombreParada;
    }

    public void setNombreParada(String nombreParada) {
        this.nombreParada = nombreParada;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public boolean isFavoritoParada() {
        return favoritoParada;
    }

    public void setFavoritoParada(boolean favoritoParada) {
        this.favoritoParada = favoritoParada;
    }

}
