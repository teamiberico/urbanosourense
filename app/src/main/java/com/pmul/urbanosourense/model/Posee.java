package com.pmul.urbanosourense.model;

import java.util.ArrayList;

/**
 * Created by DAM221 on 22/11/2017.
 */

public class Posee {

    private boolean tipoTrayecto;
    private String numeroLinea;
    private ArrayList<String> codigosParadas;

    public Posee() {}

    public Posee(boolean tipoTrayecto, String numeroLinea, ArrayList<String> codigosParadas) {
        this.tipoTrayecto = tipoTrayecto;
        this.numeroLinea = numeroLinea;
        this.codigosParadas = codigosParadas;
    }

    public boolean isTipoTrayecto() {
        return tipoTrayecto;
    }

    public void setTipoTrayecto(boolean tipoTrayecto) {
        this.tipoTrayecto = tipoTrayecto;
    }

    public String getNumeroLinea() {
        return numeroLinea;
    }

    public void setNumeroLinea(String numeroLinea) {
        this.numeroLinea = numeroLinea;
    }

    public ArrayList<String> getCodigosParadas() {
        return codigosParadas;
    }

    public void setCodigosParadas(ArrayList<String> codigosParadas) {
        this.codigosParadas = codigosParadas;
    }

}
