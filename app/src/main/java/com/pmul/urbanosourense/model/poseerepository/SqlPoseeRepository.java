package com.pmul.urbanosourense.model.poseerepository;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.pmul.urbanosourense.model.DatabaseSQLiteOpenHelper;
import com.pmul.urbanosourense.model.Posee;

import java.util.ArrayList;

/**
 * Created by dam221 on 11/12/2017.
 */

public class SqlPoseeRepository implements PoseeRepository {

    private ArrayList<Posee> posees;

    public SqlPoseeRepository(Context context) {
        DatabaseSQLiteOpenHelper helper = new DatabaseSQLiteOpenHelper(context);
        posees = new ArrayList<>();

        String query = "SELECT * FROM " + DatabaseSQLiteOpenHelper.TABLE_POSEE;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            String cod = "";

            do{
                Posee posee = new Posee();
                int tipoTrayecto = cursor.getInt(0);

                if (tipoTrayecto==0){
                    posee.setTipoTrayecto(false);
                }
                else {
                    posee.setTipoTrayecto(true);
                }

                String numeroLinea = cursor.getString(1);
                posee.setNumeroLinea(numeroLinea);

                ArrayList<String> codigosParadas = new ArrayList<>();

                if (!cod.equals("")) {
                    codigosParadas.add(cod);
                }

                while (tipoTrayecto == cursor.getInt(0) && numeroLinea.equals(cursor.getString(1))) {
                    codigosParadas.add(cursor.getString(2));

                    if (!cursor.isLast()){
                        cursor.moveToNext();
                    }
                    else{
                        break;
                    }
                }

                posee.setCodigosParadas(codigosParadas);
                cod = cursor.getString(2);
                posees.add(posee);
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
    }

    @Override
    public ArrayList<String> getCodigosParadas(boolean tipoTrayecto, String numeroLinea) {

        for (Posee p: posees) {
            if (p.isTipoTrayecto() == tipoTrayecto && p.getNumeroLinea().equals(numeroLinea)){
                return p.getCodigosParadas();
            }
        }

        return null;
    }

    @Override
    public ArrayList<String> getNumeroLinea(String codigoParada) {
        ArrayList<String> listaNumerosLinea = new ArrayList<>();

        for (Posee p: posees){
            for (String s: p.getCodigosParadas()){
                if (s.equals(codigoParada) && !listaNumerosLinea.contains(p.getNumeroLinea())){
                    listaNumerosLinea.add(p.getNumeroLinea());
                }
            }
        }

        return listaNumerosLinea;
    }
}
