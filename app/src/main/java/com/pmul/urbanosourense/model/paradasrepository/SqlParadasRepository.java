package com.pmul.urbanosourense.model.paradasrepository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pmul.urbanosourense.model.DatabaseSQLiteOpenHelper;
import com.pmul.urbanosourense.model.Parada;

import java.util.ArrayList;

/**
 * Created by dam221 on 11/12/2017.
 */

public class SqlParadasRepository implements ParadasRepository {

    private ArrayList<Parada> paradas;
    private DatabaseSQLiteOpenHelper helper;

    public SqlParadasRepository(Context context) {
        this.helper = new DatabaseSQLiteOpenHelper(context);
        paradas = new ArrayList<>();

        String query = "SELECT * FROM " + DatabaseSQLiteOpenHelper.TABLE_PARADA;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do{
                Parada parada = new Parada();
                parada.setCodigoParada(cursor.getString(0));
                parada.setNombreParada(cursor.getString(1));
                parada.setLatitud(cursor.getDouble(2));
                parada.setLongitud(cursor.getDouble(3));
                int favorito = cursor.getInt(4);
                if (favorito==0){
                    parada.setFavoritoParada(false);
                }
                else {
                    parada.setFavoritoParada(true);
                }
                paradas.add(parada);
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

    }

    @Override
    public ArrayList<Parada> getParadas() {
        return this.paradas;
    }

    @Override
    public ArrayList<Parada> getParadas(ArrayList<String> codigosParadas) {
        ArrayList<Parada> listaParadas = new ArrayList<>();

       for (String codigoParada:codigosParadas){
            for (Parada p: paradas){
                if (p.getCodigoParada().equals(codigoParada)){
                    listaParadas.add(p);
                }
            }
        }

        return listaParadas;
    }

    @Override
    public ArrayList<Parada> getParadas(String busqueda) {
        ArrayList<Parada> paradasBusqueda = new ArrayList<>();

        for (Parada parada: paradas) {
            if (parada.getCodigoParada().toUpperCase().contains(busqueda.toUpperCase()) || parada.getNombreParada().toUpperCase().contains(busqueda.toUpperCase())){
                paradasBusqueda.add(parada);
            }
        }

        return paradasBusqueda;
    }

    @Override
    public Parada getParada(String codigoParada) {

        for (Parada p: paradas) {
            if (p.getCodigoParada().equals(codigoParada)){
                return p;
            }
        }

        return null;
    }

    @Override
    public ArrayList<Parada> getParadasFavoritas() {
        ArrayList<Parada> paradasFavoritas = new ArrayList<>();

        for (Parada p: paradas) {
            if (p.isFavoritoParada())
                paradasFavoritas.add(p);
        }

        return paradasFavoritas;
    }

    @Override
    public void removeParadasFavoritos() {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues valores;

        for (Parada p: paradas) {
            p.setFavoritoParada(false);
            valores = new ContentValues();
            valores.put(DatabaseSQLiteOpenHelper.PARADA_COLUMN_FAVORITO, 0);
            db.update(DatabaseSQLiteOpenHelper.TABLE_PARADA, valores, DatabaseSQLiteOpenHelper.PARADA_COLUMN_CODIGO + " = '" + p.getCodigoParada()+"'", null);
        }

        db.close();
    }

    @Override
    public void cambiarParadaFavorito(Parada parada) {
        int favoritoParada=0;

        if (parada.isFavoritoParada()){
            favoritoParada = 1;
        }

        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put(DatabaseSQLiteOpenHelper.PARADA_COLUMN_FAVORITO, favoritoParada);
        db.update(DatabaseSQLiteOpenHelper.TABLE_PARADA, valores, DatabaseSQLiteOpenHelper.PARADA_COLUMN_CODIGO + " = '" + parada.getCodigoParada()+"'", null);
        db.close();

    }
}
