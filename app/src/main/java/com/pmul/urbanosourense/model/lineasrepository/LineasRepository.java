package com.pmul.urbanosourense.model.lineasrepository;

import com.pmul.urbanosourense.model.Linea;

import java.util.ArrayList;

/**
 * Created by DAM221 on 22/11/2017.
 */

public interface LineasRepository {

    ArrayList<Linea> getLineas();
    ArrayList<Linea> getLineas(ArrayList<String> numerosLinea);
    ArrayList<Linea> getLineas(String busqueda);
    ArrayList<Linea> getLineasFavoritas();
    void removeLineasFavoritos();
    void cambiarLineaFavorito(Linea linea);

}
