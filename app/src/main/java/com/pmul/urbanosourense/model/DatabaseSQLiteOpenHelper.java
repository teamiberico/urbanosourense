package com.pmul.urbanosourense.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by dam207 on 05/12/2017.
 */

public class DatabaseSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "urbanosOurense.db";
    public static final String TABLE_LINEA = "Linea";
    public static final String TABLE_PARADA = "Parada";
    public static final String TABLE_POSEE = "Posee";
    public static final String LINEA_COLUMN_NUMEROLINEA = "numeroLinea";
    public static final String LINEA_COLUMN_ORIGEN = "origen";
    public static final String LINEA_COLUMN_DESTINO = "destino";
    public static final String LINEA_COLUMN_COLOR = "color";
    public static final String LINEA_COLUMN_FAVORITO = "favoritoLinea";
    public static final String PARADA_COLUMN_CODIGO = "codigoParada";
    public static final String PARADA_COLUMN_NOMBRE = "nombreParada";
    public static final String PARADA_COLUMN_LATITUD = "latitud";
    public static final String PARADA_COLUMN_LONGITUD = "longitud";
    public static final String PARADA_COLUMN_FAVORITO = "favoritoParada";
    public static final String POSEE_COLUMN_TIPOTRAYECTO = "tipoTrayecto";
    public static final String POSEE_COLUMN_NUMEROLINEA = "numeroLinea";
    public static final String POSEE_COLUMN_CODIGOPARADA = "codigoParada";
    private Context context;

    public DatabaseSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.crearBD();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) { }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) { }

    private void crearBD () {
        String packageName = context.getPackageName();
        String DB_PATH = "/data/data/" + packageName + "/databases/";
        String outFileName = DB_PATH + DATABASE_NAME;

        File f = new File(outFileName);

        if(!f.exists())
        {
            File directory = new File(DB_PATH);

            if (!directory.exists()) {
                directory.mkdirs();
            }

            InputStream inputStream = null;

            try {
                inputStream = context.getAssets().open(DATABASE_NAME);
            } catch (IOException e) {
                e.printStackTrace();
            }

            OutputStream outputStream = null;

            try {
                outputStream = new FileOutputStream(outFileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            byte[] buffer = new byte[1024];
            int length;

            try {
                while ((length = inputStream.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
