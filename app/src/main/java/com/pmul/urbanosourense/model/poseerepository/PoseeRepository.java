package com.pmul.urbanosourense.model.poseerepository;

import java.util.ArrayList;

/**
 * Created by DAM221 on 22/11/2017.
 */

public interface PoseeRepository {

    ArrayList<String> getCodigosParadas(boolean tipoTrayecto, String numeroLinea);
    ArrayList<String> getNumeroLinea(String codigoParada);

}
