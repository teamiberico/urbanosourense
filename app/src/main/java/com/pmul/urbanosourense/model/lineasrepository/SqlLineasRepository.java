package com.pmul.urbanosourense.model.lineasrepository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pmul.urbanosourense.model.DatabaseSQLiteOpenHelper;
import com.pmul.urbanosourense.model.Linea;

import java.util.ArrayList;

/**
 * Created by dam207 on 05/12/2017.
 */

public class SqlLineasRepository implements LineasRepository {

    private ArrayList<Linea> lineas;
    private DatabaseSQLiteOpenHelper helper;

    public SqlLineasRepository(Context context) {
        this.helper = new DatabaseSQLiteOpenHelper(context);
        lineas = new ArrayList<>();

        String query = "SELECT * FROM " + DatabaseSQLiteOpenHelper.TABLE_LINEA;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do{
                Linea linea = new Linea();
                linea.setNumeroLinea(cursor.getString(0));
                linea.setOrigen(cursor.getString(1));
                linea.setDestino(cursor.getString(2));
                linea.setColor(cursor.getString(3));
                int favorito = cursor.getInt(4);
                if (favorito==0){
                    linea.setFavoritoLinea(false);
                }
                else {
                    linea.setFavoritoLinea(true);
                }
                lineas.add(linea);
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

    }

    @Override
    public ArrayList<Linea> getLineas() {
        return this.lineas;
    }

    @Override
    public ArrayList<Linea> getLineas(ArrayList<String> numerosLinea) {
        ArrayList<Linea> listaLineas = new ArrayList<>();

        for (String numeroLinea: numerosLinea){
            for (Linea l: lineas) {
                if (l.getNumeroLinea().equals(numeroLinea)) {
                    listaLineas.add(l);
                }
            }
        }

        return listaLineas;
    }

    @Override
    public ArrayList<Linea> getLineas(String busqueda) {
        ArrayList<Linea> lineasBusqueda = new ArrayList<>();

        for (Linea linea: lineas) {
            if (linea.getNumeroLinea().toUpperCase().contains(busqueda.toUpperCase()) || linea.getOrigen().toUpperCase().contains(busqueda.toUpperCase()) || linea.getDestino().toUpperCase().contains(busqueda.toUpperCase())){
                lineasBusqueda.add(linea);
            }
        }

        return lineasBusqueda;
    }

    @Override
    public ArrayList<Linea> getLineasFavoritas() {
        ArrayList<Linea> lineasFavoritas = new ArrayList<>();

        for (Linea l: lineas) {
            if (l.isFavoritoLinea()){
                lineasFavoritas.add(l);
            }
        }

        return lineasFavoritas;
    }

    @Override
    public void removeLineasFavoritos() {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues valores;

        for (Linea l: lineas) {
            l.setFavoritoLinea(false);
            valores = new ContentValues();
            valores.put(DatabaseSQLiteOpenHelper.LINEA_COLUMN_FAVORITO, 0);
            db.update(DatabaseSQLiteOpenHelper.TABLE_LINEA, valores, DatabaseSQLiteOpenHelper.LINEA_COLUMN_NUMEROLINEA + " = '" + l.getNumeroLinea()+"'", null);
        }

        db.close();
    }

    @Override
    public void cambiarLineaFavorito(Linea linea) {
        int favoritoLinea = 0;

        if (linea.isFavoritoLinea()){
            favoritoLinea = 1;
        }

        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues valores = new ContentValues();
        valores.put(DatabaseSQLiteOpenHelper.LINEA_COLUMN_FAVORITO, favoritoLinea);
        db.update(DatabaseSQLiteOpenHelper.TABLE_LINEA, valores, DatabaseSQLiteOpenHelper.LINEA_COLUMN_NUMEROLINEA + " = '" + linea.getNumeroLinea()+"'", null);
        db.close();
    }
}
