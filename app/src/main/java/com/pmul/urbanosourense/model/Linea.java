package com.pmul.urbanosourense.model;

/**
 * Created by Noel on 04/11/2017.
 */

public class Linea {

    private String numeroLinea;
    private String origen;
    private String destino;
    private String color;
    private boolean favoritoLinea;

    public Linea () {}

    public Linea(String numeroLinea, String origen, String destino, String color, boolean favoritoLinea) {
        this.numeroLinea = numeroLinea;
        this.origen = origen;
        this.destino = destino;
        this.color = color;
        this.favoritoLinea = favoritoLinea;
    }

    public String getNumeroLinea() {
        return numeroLinea;
    }

    public void setNumeroLinea(String numeroLinea) {
        this.numeroLinea = numeroLinea;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFavoritoLinea() {
        return favoritoLinea;
    }

    public void setFavoritoLinea(boolean favoritoLinea) {
        this.favoritoLinea = favoritoLinea;
    }

}
