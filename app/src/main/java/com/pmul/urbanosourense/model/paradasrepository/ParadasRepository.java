package com.pmul.urbanosourense.model.paradasrepository;

import com.pmul.urbanosourense.model.Parada;

import java.util.ArrayList;

/**
 * Created by DAM221 on 22/11/2017.
 */

public interface ParadasRepository {

    ArrayList<Parada> getParadas();
    ArrayList<Parada> getParadas(ArrayList<String> codigosParadas);
    ArrayList<Parada> getParadas(String busqueda);
    Parada getParada(String codigoParada);
    ArrayList<Parada> getParadasFavoritas();
    void removeParadasFavoritos();
    void cambiarParadaFavorito(Parada parada);

}
