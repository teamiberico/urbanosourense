package com.pmul.urbanosourense.model;

import android.app.Application;

import com.pmul.urbanosourense.model.lineasrepository.LineasRepository;
import com.pmul.urbanosourense.model.lineasrepository.SqlLineasRepository;
import com.pmul.urbanosourense.model.paradasrepository.ParadasRepository;
import com.pmul.urbanosourense.model.paradasrepository.SqlParadasRepository;
import com.pmul.urbanosourense.model.poseerepository.PoseeRepository;
import com.pmul.urbanosourense.model.poseerepository.SqlPoseeRepository;

/**
 * Created by DAM221 on 23/11/2017.
 */

public class ListasApplication extends Application{

    private static LineasRepository lineasRepository;
    private static ParadasRepository paradasRepository;
    private static PoseeRepository poseeRepository;

    public static LineasRepository getLineasRepository() {
        return lineasRepository;
    }

    public static ParadasRepository getParadasRepository() {
        return paradasRepository;
    }

    public static PoseeRepository getPoseeRepository() {
        return poseeRepository;
    }

    @Override
    public void onCreate() {
        lineasRepository = new SqlLineasRepository(getBaseContext());
        paradasRepository = new SqlParadasRepository(getBaseContext());
        poseeRepository = new SqlPoseeRepository(getBaseContext());

        super.onCreate();
    }
}
