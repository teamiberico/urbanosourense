package com.pmul.urbanosourense;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pmul.urbanosourense.model.Parada;
import com.pmul.urbanosourense.model.ListasApplication;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParadasFragment extends Fragment {

    private RecyclerView listaParadas;
    private ParadasListAdapter paradasListAdapter;
    private Boolean trayecto;
    private FloatingActionButton fabMapa;
    private FloatingActionButton fabInfo;
    private android.support.v7.app.ActionBar actionBar;

    public ParadasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_paradas, container, false);

        actionBar = ((MainActivity)getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Linea "+getArguments().getString("numeroLinea"));
            actionBar.setSubtitle(getArguments().getString("origenLinea")+" - "+getArguments().getString("destinoLinea"));
        }

        // Inflate the layout for this fragment

        listaParadas = view.findViewById(R.id.paradasListRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        listaParadas.setLayoutManager(layoutManager);
        listaParadas.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));

        fabMapa = view.findViewById(R.id.fabMapa);
        fabInfo = view.findViewById(R.id.fabInfo);

        fabMapa.setVisibility(View.INVISIBLE);
        fabInfo.setVisibility(View.INVISIBLE);

        if (getArguments().getString("origen").equals("lineas")) {
            fabMapa.setVisibility(View.VISIBLE);
            fabInfo.setVisibility(View.VISIBLE);

            listaParadas.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && fabInfo.getVisibility() == View.VISIBLE && fabMapa.getVisibility() == View.VISIBLE) {
                        fabInfo.hide();
                        fabMapa.hide();
                    } else if (dy < 0 && fabInfo.getVisibility() != View.VISIBLE && fabMapa.getVisibility() != View.VISIBLE) {
                        fabInfo.show();
                        fabMapa.show();
                    }
                }
            });

            fabMapa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new MapaFragment();
                    Bundle args = new Bundle();
                    args.putString("origen", "fabMapa");
                    args.putString("numeroLinea", getArguments().getString("numeroLinea"));
                    args.putBoolean("trayecto", trayecto);
                    fragment.setArguments(args);

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
                }
            });

            fabInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment = new InfoFragment();
                    Bundle args = new Bundle();
                    args.putString("numeroLinea", getArguments().getString("numeroLinea"));
                    args.putBoolean("trayecto", trayecto);
                    fragment.setArguments(args);

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
                }
            });
        }

        trayecto = true;
        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.linea_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Fragment fragment = new FavoritosFragment();
                Bundle args = new Bundle();
                args.putString("origen", "busqueda");
                args.putString("resultado",query);
                fragment.setArguments(args);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cambiarTaryecto) {

            if (trayecto) {
                trayecto = false;
                if (actionBar != null) {
                    actionBar.setSubtitle(getArguments().getString("destinoLinea")+" - "+getArguments().getString("origenLinea"));
                }
            }
            else {
                trayecto = true;
                if (actionBar != null) {
                    actionBar.setSubtitle(getArguments().getString("origenLinea")+" - "+getArguments().getString("destinoLinea"));
                }
            }
            onResume();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<Parada> paradas;

        if (getArguments().getString("origen").equals("favoritos")) {
            paradas = ListasApplication.getParadasRepository().getParadasFavoritas();
            setHasOptionsMenu(false);
            MainActivity.borrarParada = true;

            if (actionBar != null) {
                actionBar.setTitle("Favoritos");
                actionBar.setSubtitle("");
            }

        }
        else if (getArguments().getString("origen").equals("busqueda")){
            paradas = ListasApplication.getParadasRepository().getParadas(getArguments().getString("resultado"));

            if (paradas.size()== 0){
                Toast.makeText(getContext(),"No se ha encontrado ninguna parada",Toast.LENGTH_SHORT).show();
            }

            setHasOptionsMenu(false);
            MainActivity.borrarParada = false;

            if (actionBar != null) {
                actionBar.setTitle("Resultados");
                actionBar.setSubtitle("");
            }
        }

        else {
            Bundle bundle = this.getArguments();
            paradas = ListasApplication.getParadasRepository().getParadas(ListasApplication.getPoseeRepository().getCodigosParadas(trayecto, bundle.getString("numeroLinea")));
            setHasOptionsMenu(true);
            MainActivity.borrarParada = false;
        }

        paradasListAdapter = new ParadasListAdapter(paradas);
        listaParadas.setAdapter(paradasListAdapter);
        listenerParada();

    }

    public void listenerParada() {
        paradasListAdapter.setOnClickListener(new ParadasListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, Parada parada) {
                Fragment fragment = new MapaFragment();
                Bundle args = new Bundle();

                if (getArguments().getString("origen").equals("favoritos") | getArguments().getString("origen").equals("busqueda"))  {
                    args.putString("origen", "favoritos");
                }
                else {
                    args.putString("origen", "parada");
                    args.putString("numeroLinea", getArguments().getString("numeroLinea"));
                    args.putBoolean("trayecto", trayecto);
                }

                args.putString("codigoParada", parada.getCodigoParada());
                fragment.setArguments(args);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
