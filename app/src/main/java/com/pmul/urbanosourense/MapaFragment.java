package com.pmul.urbanosourense;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.GeocodedWaypoint;
import com.google.maps.model.TravelMode;
import com.pmul.urbanosourense.model.Parada;
import com.pmul.urbanosourense.model.ListasApplication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment implements OnMapReadyCallback {

    private ArrayList<Parada> listaParadas;
    private GoogleMap mMap;
    private Marker marker;
    private Marker markerRuta;
    private Polyline polyline;
    private boolean añadirRuta = false;
    private boolean moverCamara;
    private LatLng latLng;
    private boolean start = true;

    public MapaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapa, container, false);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Mapa");
            actionBar.setSubtitle("");
        }

        setHasOptionsMenu(true);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // Inflate the layout for this fragment
        startLocationUpdates();

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.checkboxNavegacion){

            if(item.isChecked()){
               item.setChecked(false);
            }
            else{
                item.setChecked(true);
            }

            moverCamara = item.isChecked();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.search_menu, menu);
        inflater.inflate(R.menu.mapa_menu, menu);
        MenuItem checkboxNavegacion = menu.findItem(R.id.checkboxNavegacion);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        checkboxNavegacion.setChecked(prefs.getBoolean("navegacionMapa", true));
        moverCamara = prefs.getBoolean("navegacionMapa", true);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }

        final SearchView finalSearchView = searchView;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    Geocoder geocoder = new Geocoder(getContext());
                    List<Address> results = geocoder.getFromLocationName(query, 1);

                    if (results.size() > 0) {

                        if (marker != null){
                            marker.remove();
                        }

                        LatLng location = new LatLng(results.get(0).getLatitude(), results.get(0).getLongitude());
                        marker = mMap.addMarker(new MarkerOptions().position(location).title(query).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
                        finalSearchView.onActionViewCollapsed();

                    }
                    else{
                        Toast.makeText(getContext() ,"No hay resultados", Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onPause() {
        super.onPause();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED &&
                    getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.zoomTo(16));
        mMap.getUiSettings().setZoomControlsEnabled(true);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        if (getArguments().getString("origen").equals("mapa")){
            listaParadas = ListasApplication.getParadasRepository().getParadas();
        }
        else if (getArguments().getString("origen").equals("fabMapa") | getArguments().getString("origen").equals("parada")){
            listaParadas = ListasApplication.getParadasRepository().getParadas(ListasApplication.getPoseeRepository().getCodigosParadas(getArguments().getBoolean("trayecto"), getArguments().getString("numeroLinea")));
            com.google.maps.model.LatLng[] waypoints = new com.google.maps.model.LatLng[listaParadas.size()-1];

            for (int i = 0; i< listaParadas.size()-1; i++) {
                waypoints[i] = new com.google.maps.model.LatLng(listaParadas.get(i).getLatitud(), listaParadas.get(i).getLongitud());
            }

            try {
                DirectionsApiRequest result = DirectionsApi.newRequest(getGeoContext())
                        .mode(TravelMode.DRIVING).origin(new com.google.maps.model.LatLng(listaParadas.get(0).getLatitud(), listaParadas.get(0).getLongitud()))
                        .waypoints(waypoints)
                        .destination(new com.google.maps.model.LatLng(listaParadas.get(listaParadas.size()-1).getLatitud(), listaParadas.get(listaParadas.size()-1).getLongitud()));

                DirectionsResult r = result.await();
                addPolyline(r,mMap,false);

            } catch (ApiException | InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        else if (getArguments().getString("origen").equals("favoritos")) {
            listaParadas = new ArrayList<>();
            listaParadas.add(ListasApplication.getParadasRepository().getParada(getArguments().getString("codigoParada")));
        }

        for (Parada p: listaParadas) {
            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(p.getLatitud(),p.getLongitud())).title(p.getNombreParada()).snippet("Código: "+p.getCodigoParada()));

            if ((getArguments().getString("origen").equals("parada") | getArguments().getString("origen").equals("favoritos"))
                    && p.getCodigoParada().equals(getArguments().getString("codigoParada"))) {
                marker.showInfoWindow();
                markerRuta = marker;
                añadirRuta = true;
            }
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                añadirRuta(marker);
                return false;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String codigo = marker.getSnippet();

                if (codigo!=null) {
                    String[]partes = codigo.split(": ");
                    Fragment fragment = new LineasFragment();
                    Bundle args = new Bundle();
                    args.putString("origen", "mapa");
                    args.putString("codigo", partes[1]);
                    fragment.setArguments(args);
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
                }

            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (polyline!=null) {
                    polyline.remove();
                }
            }
        });

    }

    private void añadirRuta (Marker marker) {
        try {
            double latitude = latLng.latitude;
            double longitud = latLng.longitude;

            DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                    .mode(TravelMode.WALKING).origin(new com.google.maps.model.LatLng(latitude,longitud))
                    .destination(new com.google.maps.model.LatLng(marker.getPosition().latitude,marker.getPosition().longitude))
                    .await();

            addPolyline(result,mMap,true);

        } catch (ApiException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private GeoApiContext getGeoContext() {

        return new GeoApiContext.Builder()
                .queryRateLimit(3).apiKey(getString(R.string.directions_api_key))
                .connectTimeout(1, TimeUnit.SECONDS).readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS).build();
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap, boolean origen) {

        List<com.google.maps.model.LatLng> decodedPath = results.routes[0].overviewPolyline.decodePath();
        ArrayList<LatLng> points = new ArrayList<>();

        for (com.google.maps.model.LatLng ll : decodedPath) {
            points.add(new LatLng(ll.lat, ll.lng));
        }

        if (origen) {
            if (polyline!=null){
                polyline.remove();
            }
            polyline = mMap.addPolyline(new PolylineOptions().addAll(points).color(Color.argb(150,43,97,253)));
        }
        else{
            mMap.addPolyline(new PolylineOptions().addAll(points).color(Color.argb(150,255,0,0)));
        }

    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        long UPDATE_INTERVAL = 10 * 1000;
        long FASTEST_INTERVAL = 5 * 1000;
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(getActivity()).requestLocationUpdates(locationRequest
                , new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }

    public void onLocationChanged(Location location) {
        // New location has now been determined
        // You can now create a LatLng Object for use with maps
        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        if (moverCamara || start){
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            if (start) {
                start = false;
            }
        }
        if (añadirRuta){
            añadirRuta(markerRuta);
            añadirRuta = false;
        }
    }

}
