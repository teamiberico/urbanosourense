package com.pmul.urbanosourense;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pmul.urbanosourense.model.ListasApplication;
import com.pmul.urbanosourense.model.Parada;

import java.util.ArrayList;

/**
 * Created by DAM221 on 24/11/2017.
 */

public class ParadasListAdapter extends RecyclerView.Adapter<ParadasListAdapter.ParadasViewHolder>{

    private ArrayList<Parada> paradas;
    private View view;
    private OnItemClickListener listener;

    ParadasListAdapter(ArrayList<Parada> paradas) {
        this.paradas = paradas;
    }

    @Override
    public ParadasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parada, parent, false);
        return new ParadasViewHolder(view);
    }

    public interface OnItemClickListener{
        void onItemClick(View itemView, Parada parada);
    }

    void setOnClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(final ParadasViewHolder holder, final int position) {
        holder.codigoParada.setText(String.valueOf(paradas.get(position).getCodigoParada()));
        holder.nombreParada.setText(paradas.get(position).getNombreParada());

        GradientDrawable shape = (GradientDrawable)holder.codigoParada.getBackground();
        shape.setColor(view.getResources().getColor(R.color.fondoParada));

        if (paradas.get(position).isFavoritoParada()) {
            holder.favoritoParada.setBackgroundResource(R.drawable.ic_action_favorito);
        }
        else {
            holder.favoritoParada.setBackgroundResource(R.drawable.ic_action_nofavorito);
        }

        holder.favoritoParada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (paradas.get(position).isFavoritoParada()) {
                    holder.favoritoParada.setBackgroundResource(R.drawable.ic_action_nofavorito);
                    paradas.get(position).setFavoritoParada(false);
                    ListasApplication.getParadasRepository().cambiarParadaFavorito(paradas.get(position));
                    if (MainActivity.borrarParada) {
                        removeParada(paradas.get(position));
                    }
                }
                else {
                    holder.favoritoParada.setBackgroundResource(R.drawable.ic_action_favorito);
                    paradas.get(position).setFavoritoParada(true);
                    ListasApplication.getParadasRepository().cambiarParadaFavorito(paradas.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return paradas.size();
    }

    public final class ParadasViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView codigoParada, nombreParada;
        private ImageButton favoritoParada;

        public ParadasViewHolder(View itemView) {
            super(itemView);
            codigoParada = itemView.findViewById(R.id.codigoParadaTextView);
            nombreParada = itemView.findViewById(R.id.nombreParadaTextView);
            favoritoParada = itemView.findViewById(R.id.favoritoParadaImageButton);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null){
                listener.onItemClick(itemView, paradas.get(getAdapterPosition()));
            }
        }
    }

    private void removeParada(Parada parada) {
        int pos = paradas.indexOf(parada);
        paradas.remove(pos);
        notifyDataSetChanged();
    }

}
