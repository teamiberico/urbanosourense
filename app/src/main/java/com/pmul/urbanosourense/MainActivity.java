package com.pmul.urbanosourense;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_MAPS = 0;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private SharedPreferences prefs;
    public static boolean borrarLinea = true;
    public static boolean borrarParada = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_MAPS);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.startFragment(0);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_MAPS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

//    public void setDrawerEnabled(boolean enabled) {
//        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
//                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
//        drawer.setDrawerLockMode(lockMode);
//        toggle.setDrawerIndicatorEnabled(enabled);
//    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        }
        else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    private void startFragment (int indice) {
        Fragment fragment = new Fragment();
        Bundle args = new Bundle();

        if (indice == 0) {
            this.startFragment(Integer.parseInt(prefs.getString("ventanaInicio","1")));
        } else if (indice == 1) {
            fragment = new LineasFragment();
            args.putString("origen", "lineas");
            fragment.setArguments(args);
        } else if (indice == 2) {
            fragment = new MapaFragment();
            args.putString("origen", "mapa");
            fragment.setArguments(args);
        } else if (indice == 3) {
            fragment = new FavoritosFragment();
            args.putString("origen", "favoritos");
            fragment.setArguments(args);
        } else if (indice == 4) {
            fragment = new TarifasFragment();
        } else if (indice == 5) {
            Intent intent = new Intent(this, ConfiguracionActivity.class);
            startActivity(intent);
        } else if (indice == 6) {
            fragment = new AcercaDeFragment();
        }

        if (indice != 5 && indice != 0) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentPlaceHolder, fragment).addToBackStack(null).commit();
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_inicio) {
            this.startFragment(0);
        } else if (id == R.id.nav_lineas) {
            this.startFragment(1);
        } else if (id == R.id.nav_mapa) {
            this.startFragment(2);
        } else if (id == R.id.nav_favoritos) {
            this.startFragment(3);
        } else if (id == R.id.nav_tarifas) {
            this.startFragment(4);
        } else if (id == R.id.nav_configuracion) {
            this.startFragment(5);
        }
        else if (id == R.id.nav_acercaDe) {
            this.startFragment(6);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

}
