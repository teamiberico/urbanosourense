package com.pmul.urbanosourense;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pmul.urbanosourense.model.Linea;
import com.pmul.urbanosourense.model.ListasApplication;

import java.util.ArrayList;

/**
 * Created by DAM221 on 22/11/2017.
 */

public class LineasListAdapter extends RecyclerView.Adapter<LineasListAdapter.LineasViewHolder>{

    private ArrayList<Linea> lineas;
    private View view;
    private OnItemClickListener listener;

    LineasListAdapter(ArrayList<Linea> lineas) {
        this.lineas = lineas;
    }

    @Override
    public LineasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_linea,parent,false);

        return new LineasViewHolder(view);
    }

    public interface OnItemClickListener{
        void onItemClick(View itemView, Linea linea);
    }

    public void setOnClickListener (OnItemClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(final LineasViewHolder holder, final int position) {

        String numeroLinea = lineas.get(position).getNumeroLinea();
        holder.numeroLinea.setText(lineas.get(position).getNumeroLinea());

        if (numeroLinea.equals("1") || numeroLinea.equals("7")){
            holder.numeroLinea.setTextColor(view.getResources().getColor(R.color.colorNegro));
        }

        GradientDrawable shape = (GradientDrawable)holder.numeroLinea.getBackground();
        shape.setColor(view.getContext().getResources().getColor(view.getResources().getIdentifier(lineas.get(position).getColor(), "color", "com.pmul.urbanosourense")));
        holder.origenLinea.setText(lineas.get(position).getOrigen());
        holder.destinoLinea.setText(lineas.get(position).getDestino());

        if (lineas.get(position).isFavoritoLinea()) {
            holder.favoritoLinea.setBackgroundResource(R.drawable.ic_action_favorito);
        }
        else {
            holder.favoritoLinea.setBackgroundResource(R.drawable.ic_action_nofavorito);
        }

        holder.favoritoLinea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (lineas.get(position).isFavoritoLinea()) {
                    holder.favoritoLinea.setBackgroundResource(R.drawable.ic_action_nofavorito);
                    lineas.get(position).setFavoritoLinea(false);
                    ListasApplication.getLineasRepository().cambiarLineaFavorito(lineas.get(position));
                    if (MainActivity.borrarLinea) {
                        removeLinea(lineas.get(position));
                    }
                }
                else {
                    holder.favoritoLinea.setBackgroundResource(R.drawable.ic_action_favorito);
                    lineas.get(position).setFavoritoLinea(true);
                    ListasApplication.getLineasRepository().cambiarLineaFavorito(lineas.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return lineas.size();
    }

    public final class LineasViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView numeroLinea, origenLinea, destinoLinea;
        private ImageButton favoritoLinea;

        LineasViewHolder(View itemView) {
            super(itemView);
            numeroLinea = itemView.findViewById(R.id.numeroLineaTextView);
            origenLinea = itemView.findViewById(R.id.origenLineaTextView);
            destinoLinea = itemView.findViewById(R.id.destinoLineaTextView);
            favoritoLinea = itemView.findViewById(R.id.favoritoLineaImageButton);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onItemClick(itemView, lineas.get(getAdapterPosition()));
            }
        }
    }

    private void removeLinea(Linea linea) {
        int pos = lineas.indexOf(linea);
        lineas.remove(pos);
        notifyDataSetChanged();
    }

}
