package com.pmul.urbanosourense;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment {

    public InfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        ImageView banner = view.findViewById(R.id.bannerImageView);
        ImageView trayecto = view.findViewById(R.id.trayectoImageView);
        ImageView h1 = view.findViewById(R.id.h1ImageView);
        ImageView h2 = view.findViewById(R.id.h2ImageView);
        ImageView h3 = view.findViewById(R.id.h3ImageView);
        TextView  t1 = view.findViewById(R.id.h1TextView);
        TextView  t2 = view.findViewById(R.id.h2TextView);
        TextView  t3 = view.findViewById(R.id.h3TextView);

        android.support.v7.app.ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Información");
        }

        String numeroLinea = getArguments().getString("numeroLinea");
        String tipoTrayecto;

        if (getArguments().getBoolean("trayecto")){
            tipoTrayecto = "ida";
        }
        else {
            tipoTrayecto = "vuelta";
        }

        int hLaboral = (getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_horario_l", "drawable", getActivity().getPackageName()));
        int hLaboralSabado = (getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_horario_ls", "drawable", getActivity().getPackageName()));
        int hLaboralSabadoDomingoFesitvo = (getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_horario_lsdf", "drawable", getActivity().getPackageName()));
        int hSabado = (getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_horario_s", "drawable", getActivity().getPackageName()));
        int hSabadoDomingoFestivo = (getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_horario_sdf", "drawable", getActivity().getPackageName()));
        int hDomingoFestivo = (getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_horario_df", "drawable", getActivity().getPackageName()));

        banner.setImageResource(getResources().getIdentifier("linea"+numeroLinea+"_banner", "drawable", getActivity().getPackageName()));

        if (hLaboral != 0){
            h1.setImageResource(hLaboral);
            t1.setText("- LABORAL -");
        } else if (hLaboralSabado != 0) {
            h1.setImageResource((hLaboralSabado));
            t1.setText("- LABORAL Y SÁBADOS -");
        } else if (hLaboralSabadoDomingoFesitvo != 0){
            h1.setImageResource(hLaboralSabadoDomingoFesitvo);
            t1.setText("- LABORAL, SÁBADOS, DOMINGOS Y FESTIVOS -");
        }

        if (hSabado != 0){
            h2.setVisibility(View.VISIBLE);
            h2.setImageResource(hSabado);
            t2.setVisibility(View.VISIBLE);
            t2.setText("- SÁBADOS -");
        } else if (hSabadoDomingoFestivo != 0){
            h2.setVisibility(View.VISIBLE);
            h2.setImageResource(hSabadoDomingoFestivo);
            t2.setVisibility(View.VISIBLE);
            t2.setText("- SÁBADOS, DOMINGOS Y FESTIVOS -");
        }

        if (hDomingoFestivo != 0){
            if (hSabado != 0){
                h3.setVisibility(View.VISIBLE);
                h3.setImageResource(hDomingoFestivo);
                t3.setVisibility(View.VISIBLE);
                t3.setText("- DOMINGOS Y FESTIVOS -");
            }
            else{
                h2.setVisibility(View.VISIBLE);
                h2.setImageResource(hDomingoFestivo);
                t2.setVisibility(View.VISIBLE);
                t2.setText("- DOMINGOS Y FESTIVOS -");
            }
        }

        trayecto.setImageResource(getResources().getIdentifier("linea"+numeroLinea+"_"+tipoTrayecto+"_trayecto", "drawable", getActivity().getPackageName()));

        return view;
    }

}
