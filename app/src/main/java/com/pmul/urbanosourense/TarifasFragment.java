package com.pmul.urbanosourense;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TarifasFragment extends Fragment {

    public TarifasFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tarifas, container, false);

        TextView enlaceTarifas = view.findViewById(R.id.enlaceTarifas_TextView);
        enlaceTarifas.setMovementMethod(LinkMovementMethod.getInstance());

        String text = "Para más información clica <a href='http://www.urbanosdeourense.es/php/index.php?pag=tarifas/tarifas&tarifa=17'>aquí</a>";
        enlaceTarifas.setText(Html.fromHtml(text));

        android.support.v7.app.ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Tarifas");
            actionBar.setSubtitle("");
        }

        return view;
    }

}
