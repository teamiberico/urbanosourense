package com.pmul.urbanosourense;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcercaDeFragment extends Fragment {

    public AcercaDeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        android.support.v7.app.ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setTitle("Acerca de");
            actionBar.setSubtitle("");
        }

        return inflater.inflate(R.layout.fragment_acerca_de, container, false);
    }

}
